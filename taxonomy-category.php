<?php

get_header(); 
/* $term =  get_query_var( 'term' );
echo $term ; */ 
//echo post_type_archive_title( '', false );
 $title = single_tag_title( '', false );
//echo get_query_var( 'term' );
$term = get_term( 1 , 'category' );
echo $term->slug;
?>
<div class="section_wrap  child  text_center normal_box dark_scheme">
<div class="section_content media_content row">
    <div class="grid_container " id="video_grid" data-current="1" data-total="0">
                <div class="grid_controller hidden" id="$post->post_name" >
                    <div class="categories p-auto m-auto d-flex align-items-end flex-column">
                       <div class="category pt-1 pb-1 " data-cat="<?php echo $term->slug; ?>">
                            <a href="#"><?php echo $term->slug; ?></a>
                        </div>
                    </div>
                </div>
                <div class="row justify-content-md-center" id="loadvideos" style="width:100%;"></div>
      
                <a class="prev previuos_posts" id="prev_projectpage"  href="#">
                    <span class="icon-wrap">
                        <svg class="icon" width="32" height="32" viewBox="0 0 64 64">
                            <use xlink:href="#arrow-left-1">
                        </svg>
                    </span>
                </a>
                <a class="next next_posts" id="next_projectpage" href="#">
                    <span class="icon-wrap">
                        <svg class="icon" width="32" height="32" viewBox="0 0 64 64">
                            <use xlink:href="#arrow-right-1">
                        </svg>
                    </span>
                </a>
                <nav class="arrow_nav nav-slide col-md-6 col-lg-4" id="post_controller"> </nav>
        </div>
    </div>
    </div>
<?php get_footer();
?>
