(() => {
  "use strict";

  /**************** gulpfile.js configuration ****************/

  const // development or production
    devBuild =
      (process.env.NODE_ENV || "development").trim().toLowerCase() ===
      "development",
    // directory locations
    dir = {
      src: "admin/assets",
      build: "dist/"
    },

    gulp = require("gulp"),
    path = require("path"),
    browserify = require("browserify"),
    source = require("vinyl-source-stream"),
    buffer = require("vinyl-buffer"),
    babelify = require("babelify"),
    include = require("gulp-include"),
    sass = require("gulp-sass"),
    postcss = require("gulp-postcss"),
    argv = require("yargs").argv,
    gulpif = require("gulp-if"),
    autoprefixer = require("autoprefixer"),
    babel = require("gulp-babel"),
    uglify = require("gulp-uglify"),
    postcssFlex = require("postcss-flexbugs-fixes"),
    postcssSvg = require("postcss-svg"),
    notify = require("gulp-notify"),
    cssnano = require("gulp-cssnano"),
    rename = require("gulp-rename"),
    browserSync   = require('browser-sync').create(),
    clean = require("gulp-clean"),
    sourcemaps = devBuild ? require("gulp-sourcemaps") : null;

  gulp.task("browser-sync", function () {
    browserSync.init({
      server: {
        baseDir: "./dist"
      },
      notify: true,
      open: false
    });
  });

  gulp.task("styles", function () {
    var processors = [
      autoprefixer(),
      postcssFlex()
    ];
    return gulp
      .src("src/scss/style.scss")
      .pipe(sourcemaps.init())
      .pipe(gulpif(!argv.production, sourcemaps.init()))
      .pipe(
        sass({ includePaths: ["node_modules"] }).on("error", notify.onError())
      )
      .pipe(postcss(processors).on("error", notify.onError()))
      .pipe(cssnano())
      .pipe(gulpif(!argv.production, sourcemaps.write()))
      .pipe(rename("main.min.css"))
      .pipe(gulp.dest("core/"))
      .pipe(browserSync.stream());
  });

  gulp.task("js", function () {
    return browserify({
      entries: "src/js/main.js",
      paths: ["node_modules", "src/js"],
      debug: false
    })
    //   .src("admin/assets/**/*.js")
      //.transform("babelify", { presets: ["@babel/env"] })
      .bundle()
      .pipe(source("bundle.js"))
      .pipe(buffer())
      .pipe(gulpif(!argv.production, sourcemaps.init()))
      /* .pipe(
        uglify().on(
          "error",
          notify.onError(function (error) {
            return "Message to the notifier: " + error.message;
          })
        )
      ) */
      .pipe(rename("main.min.js"))
      .pipe(gulpif(!argv.production, sourcemaps.write()))
      .pipe(gulp.dest("core/"));
  });

  gulp.task("watch", function () {
    gulp.watch("src/scss/**/*.scss", gulp.parallel("styles"));
    gulp.watch("src/js/**/*.js", gulp.parallel("js"));
    gulp.watch("src/js/**.js").on("change", browserSync.reload);
    //gulp.watch("admin/dist/**/*.php").on("change", browserSync.reload);
  
  });

  //Default Gulp task

  gulp.task(
    "default",
    gulp.series(
      gulp.parallel("styles", "js"),
      gulp.parallel("browser-sync", "watch")
    )
  );

  console.log("Gulp", devBuild ? "development" : "production", "build");
})();
