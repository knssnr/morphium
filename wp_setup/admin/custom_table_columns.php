<?php
add_filter('manage_edit-category_columns', 'add_new_category_columns');

function add_new_category_columns($gallery_columns) {
	
	$new_columns['cb'] = '<input type="checkbox" />';
    $new_columns['title'] = _x('Event Title', 'column name');
    $new_columns['project'] = __('Project');
    $new_columns['formats'] = __('Formats');
	$new_columns['tags'] = __('Tags');
	$new_columns['room'] = __('Room');
    $new_columns['start_date'] = __('Start Date');
  //  $new_columns['date'] = _x('Date', 'column name');

    return $new_columns;
}
?>