<?php

/**
 * Testimonial Block Template.
 *
 * @param   array $block The block settings and attributes.
 * @param   string $content The block inner HTML (empty).
 * @param   bool $is_preview True during AJAX preview.
 * @param   (int|string) $post_id The post ID this block is saved to.
 */

// Create id attribute allowing for custom "anchor" value.
$id = 'testimonial-' . $block['id'];
if( !empty($block['anchor']) ) {
    $id = $block['anchor'];
}

// Create class attribute allowing for custom "className" and "align" values.
$className = 'testimonial';
if( !empty($block['className']) ) {
    $className .= ' ' . $block['className'];
}
if( !empty($block['align']) ) {
    $className .= ' align' . $block['align'];
}

$is_selected = false;
$cat_slug;

$title = get_field('title') ;
$category = get_field('block_cat');

$background_color = get_field('background_color');
$text_color = get_field('text_color');

if ($category){
    $cat_slug = $category->slug;
?>
<div id="<?php echo esc_attr($id); ?>" class="<?php echo esc_attr($className); ?>">
    <h4><?php echo $title;?></h4>
  
    <div class="section_content media_content row">
        <div class="grid_container container" id="video_grid" data-current="1" data-total="0">
                
                        <div class="grid_controller" id="video_post_filter" >
                            <div class="categories p-auto m-auto d-flex align-items-end flex-column">
                                <?php $categories = get_categories(array( 'hide_empty' => 0, 'orderby' => 'name', 'order' => 'ASC', 'meta_key' => 'ba_checkbox_field_id' )); ?>
                                    <?php foreach ($categories as $category): ?>
                                        <div class="category pt-1 pb-1 <?php  if($cat_slug === $category->slug ) { echo 'selected '; $is_selected = true;} ?>" data-cat="<?php echo $category->slug; ?>">
                                            <a href="#">
                                                <?php echo $category->name; ?>
                                            </a>
                                        </div>
                                    <?php endforeach;?>
                                    <div class="category pt-1 pb-1 mt-auto <?php if($is_selected === false) echo 'selected active'; ?>" data-cat="">
                                        <a href="#"> Alle </a>
                                    </div>
                            </div>
                        </div>

                        <div class="row justify-content-md-center" id="loadvideos"></div>
            
                        <a class="prev previuos_posts" id="prev_projectpage"  href="#">
                            <span class="icon-wrap">
                                <svg class="icon" width="32" height="32" viewBox="0 0 64 64">
                                    <use xlink:href="#arrow-left-1">
                                </svg>
                            </span>
                        </a>
                        <a class="next next_posts" id="next_projectpage" href="#">
                            <span class="icon-wrap">
                                <svg class="icon" width="32" height="32" viewBox="0 0 64 64">
                                    <use xlink:href="#arrow-right-1">
                                </svg>
                            </span>
                        </a>


                        <nav class="arrow_nav nav-slide col-md-6 col-lg-4" id="post_controller"> </nav>
                </div>
            </div>


    <style type="text/css">
        #<?php echo $id; ?> {
            background: <?php echo $background_color; ?>;
            color: <?php echo $text_color; ?>;
        }
    </style>
    <?php };?>
</div>