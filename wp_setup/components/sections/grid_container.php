<?php
 global $post;
 $is_selected = false;
 $cat_slug;?>

<?php   if(get_sub_field('category') ):
            $cat_slug = get_sub_field('category'); 
            $cat_slug  = $cat_slug->slug;
        elseif (get_field('side_cat')):
            $cat_slug = get_field('side_cat');
             $cat_slug = $cat_slug->slug;
        else:
            $cat_slug = $post->post_name;
        endif; ?>

<?php 
if ( is_front_page() ) :?>
 <div class="section_content media_content row">
<?php else : ?>
    <div class="section_content media_content row" id="media_content_section" style="display:none; height:0;">
<?php endif;
?>

    <div class="grid_container container" id="video_grid" data-current="1" data-total="0">

        <div class="grid_controller" id="video_post_filter">
            <div class="categories p-auto m-auto d-flex align-items-end flex-column">
                <?php $categories = get_categories(array( 'hide_empty' => 0, 'orderby' => 'name', 'order' => 'ASC', 'meta_key' => 'ba_checkbox_field_id' )); ?>
                <?php foreach ($categories as $category): ?>
                <div class="category pt-1 pb-1 <?php  if($cat_slug === $category->slug ) { echo 'selected '; $is_selected = true;} ?>"
                    data-cat="<?php echo $category->slug; ?>">
                    <a href="#">
                        <?php echo $category->name; ?>
                    </a>
                </div>
                <?php endforeach;?>
                <div class="category pt-1 pb-1 mt-auto <?php if($is_selected === false) echo 'selected active'; ?>"
                    data-cat="">
                    <a href="#"> Alle </a>
                </div>
            </div>
        </div>

        <div class="row justify-content-md-center" id="loadvideos"></div>


            <a class="prev previuos_posts hidden id="prev_projectpage" href="#"><i
                    class="fas fa-caret-square-left disabled"></i></a>
            <a class="next next_posts hidden" id="next_projectpage" href="#"><i class="fas fa-caret-square-right"></i></a>

    

        <nav id="grid_pagination">
            <div class="container">
                <a class="prev previuos_posts" id="prev_projectpage" href="#"><i
                        class="fas fa-caret-square-left disabled"></i></a>

                <div id="dots"></div>
                <a class="next next_posts" id="next_projectpage" href="#"><i class="fas fa-caret-square-right"></i></a>
                <div id="page_counter" class="hidden"></div>
            </div>

        </nav>

        <nav class="arrow_nav nav-slide col-md-6 col-lg-4" id="post_controller"> </nav>
    </div>
</div>