<?php
  if (get_sub_field('preload_video') === "Youtube") {
      $video_img = "https://img.youtube.com/vi/" . get_sub_field('preload_youtube_id') . "/maxresdefault.jpg";
      $video = "https://www.youtube.com/embed/" . get_sub_field('preload_youtube_id') . "?rel=0&controls=0&showinfo=0";
  } else {
      $video_img = get_sub_field('preview_image')['url'];
      $video = get_sub_field('preload_local')['url'];
  }

  if (get_sub_field('demoreel') === "Youtube") {
      $demoreel_img = "https://img.youtube.com/vi/" . get_sub_field('demoreel_youtube_id') . "/maxresdefault.jpg";
      $demoreel = "https://www.youtube.com/embed/" . get_sub_field('demoreel_youtube_id');
  } else {
      $demoreel_img = get_sub_field('preview_image')['url'];
      $demoreel = get_sub_field('demoreel_local_media')['url'];
  }
?>

	<div class="section_content demoreel carousel carousel-fade" id="prime_slider">
		<div class="carousel-inner">
		 <div class="carousel-item active" id="mainslide">
				<a class="jumper" id="main_back_btn" data-target="#prime_slider" data-slide-to="0" href="#">
					<span>Zurück</span>
				</a>

			
				<video id="" autoplay loop muted playsinline poster="<?php echo $video_img; ?>">
					<source src="<?php echo $video; ?>" type='video/mp4'> Your browser does not support the video tag.
				</video>
			</div>
			<!-- carousel-item -->	

			<div class="carousel-item unloaded" id="next_controll">

				<a class="jumper" id="demoreel_jumper" data-target="#prime_slider" data-slide-to="1" href="#">
					<span>Zum Demoreel</span>
					<div class="go_right_and_play icon-wrap">
						<svg class=" icon" width="32" height="32" viewBox="0 0 64 64">
							<use xlink:href="#arrow-right-3">
						</svg>
					</div>


				</a>
				<img class="affenhänger front_image " src="<?php bloginfo('template_url'); ?>/src/img/affenhänger.png" alt="affenhänger.png"  />

				<iframe class="youtube_embed" id="demoreel_video" height="100%" width="100%" frameborder="0" scrolling="no"  allow="autoplay;" allowfullscreen  data-src="<?php echo $demoreel; ?>?autoplay=1&autohide=1&yt:stretch=16:9&showinfo=0&rel=0&modestbranding=1"></iframe>

			</div> 
			<!-- carousel-item -->
		</div>
		<!-- carousel-inner -->
	</div>
	<!-- inner_section -->
