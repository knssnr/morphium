<div class="section_content section_wrap " id="client_slider" >
  <?php
        $args = array(
          'post_type'=> array('kunden'),
            'post_status' => array('publish')
        );
        $the_query = new WP_Query($args);
        $cl = 0;
        if ($the_query->have_posts()) {
            while ($the_query->have_posts()) {
                $the_query->the_post();
                $small_image = wp_get_attachment_image_src(get_post_thumbnail_id(), 'thumbnail');
                $cl++; ?>
                    <div class="col ">
                      <figure data-tilt  class="kunde open_modal visible">
                          <img class="kunden_logo" src="<?php echo $small_image[0]; ?>" alt="Kunde : <?php the_title(); ?>"/>
                      </figure>
                    </div>

            <?php  } ?>

        <?php } ?>
        <?php wp_reset_query(); ?>


 <?php wp_reset_postdata(); ?>

    </div>
