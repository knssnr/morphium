<?php while (have_posts()) : the_post(); ?>
    <section class="main-panel">
        <div <?php hybrid_attr('content section_wrap '); ?>>
            
            <div class="entry-content container">
                <div class="col-sm-12 col-md-12">

                <?php the_content(); ?>
                </div>
            </div>
        </div>
    </section>
<?php endwhile; ?>