<?php
$section_title = get_sub_field ('title');
if($section_title){
    $section_id = sonderzeichen(str_replace(' ', '_', strtolower($section_title)));
}else{
    $section_id = "section_".$r;
}


$align = get_sub_field('align');
$size = get_sub_field('size');
$style = get_sub_field('style');
$height = get_sub_field('height');
//$child_attribute = get_sub_field('child_attribute');
//$child_custome_classes = get_sub_field('custome_classes_child');
$child_classes = "child ";


//$parent_attribute = get_sub_field('parent_attribute');
//$parent_custome_classes = get_sub_field('custome_classes_parent');
$parent_classes = "parent";
if($height){
   $parent_classes = $height . ' ' . $parent_classes;
}
if ($align) $child_classes = $child_classes . " text_" . $align; 
if ($size) $child_classes = $child_classes . " " . $size . "_box"; 
if ($style) $child_classes = $child_classes . " " . $style . "_scheme"; ?>
            
 <section class="main <?php echo $parent_classes; ?>"  id="<?php echo $section_id; ?>" >
    <div class="section_wrap  <?php echo $child_classes; ?>">
    <?php include(locate_template('wp_setup/components/sections/parts/section_header.php')); ?>
  
            <?php if (have_rows('content')) : ?>
                <?php while (have_rows('content')) : the_row(); ?>
                
                 
                    <?php if (get_row_layout()) : ?>
                        <?php $id = sonderzeichen(get_row_layout()); ?>

                
                                <?php include(locate_template('wp_setup/components/sections/' . get_row_layout() . '.php')); ?>
                           
                    <?php endif; ?>  
                    
                <?php endwhile; ?>
            <?php else : ?>
            <?php endif; ?> <!-- have_rows('content')  -->
            </div>
 </section>