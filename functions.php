<?php
/* Sets the path to the core framework directory. */
if ( !defined( 'HYBRID_DIR' ) )
    define( 'HYBRID_DIR', trailingslashit( TEMPLATEPATH ) . '/core/hybrid/' );

/* Sets the path to the core framework directory URI. */
if ( !defined( 'HYBRID_URI' ) )
    define( 'HYBRID_URI', trailingslashit( TEMPLATEPATH ) . '/core/hybrid/hybrid.php' );
/* Load the core theme framework. */
require_once( trailingslashit( TEMPLATEPATH ) . '/core/hybrid/hybrid.php' );
//$theme = new Hybrid();

/**
 * Theme setup function.  This function adds support for theme features and defines the default theme
 * actions and filters.
 *
 * @since 0.1.0
 */



add_action( 'after_setup_theme', 'complex_theme_setup' );
function complex_theme_setup() {
    add_theme_support( 'hybrid-core-widgets' );
    add_theme_support( 'hybrid-core-template-hierarchy' );
    add_theme_support( 'loop-pagination' );
    add_theme_support( 'get-the-image' );
    add_theme_support( 'post-thumbnails' );
    add_theme_support( 'automatic-feed-links' );
    add_theme_support( 'cleaner-caption' );
    add_theme_support( 'wp-block-styles' );
    add_theme_support( 'cleaner-gallery' );
    add_theme_support ('align-wide');
    add_theme_support( 'responsive-embeds' );
    add_theme_support( 'editor-styles' );
add_theme_support( 'dark-editor-style' );
    add_theme_support( 'hybrid-core-theme-settings' );
}


add_action( 'after_setup_theme', 'complex_additional_setup' );
function complex_additional_setup() {
        require_once( trailingslashit( TEMPLATEPATH ) . 'wp_setup/admin/admin_settings.php' );
        require_once( trailingslashit( TEMPLATEPATH ) . 'wp_setup/sidebars/sidebar_settings.php' );
        require_once( trailingslashit( TEMPLATEPATH ) . 'wp_setup/menus/menu_settings.php' );
        require_once( trailingslashit( TEMPLATEPATH ) . 'wp_setup/admin/custom_post-types.php');
        require_once( trailingslashit( TEMPLATEPATH ) . 'wp_setup/admin/style.php');
        require_once( trailingslashit( TEMPLATEPATH ) . 'wp_setup/admin/admin_pages.php' );
        require_once( trailingslashit( TEMPLATEPATH ) . 'wp_setup/admin/widgets.php' );
        require_once( trailingslashit( TEMPLATEPATH ) . 'wp_setup/admin/featured_image.php');
        require_once( trailingslashit( TEMPLATEPATH ) . 'wp_setup/admin/custom_field.php');
        require_once( trailingslashit( TEMPLATEPATH ) . 'wp_setup/components/ajax_query.php' );
        require_once( trailingslashit(TEMPLATEPATH) . 'wp_setup/core/dev-helpers.php');
        require_once(trailingslashit( TEMPLATEPATH) . 'wp_setup/admin/acfs.php');

        if (strpos($_SERVER['SERVER_NAME'], '.de') === true) {}

    }

add_action('wp_enqueue_scripts', 'complex_add_scripts');
function complex_add_scripts() {

    wp_enqueue_script('bootstrap', 'https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js', array('jquery'),'0.1.0' , true);

    //wp_enqueue_script('jquery-ui', 'http://ajax.googleapis.com/ajax/libs/jqueryui/1.9.1/jquery-ui.min.js', array('jquery'));
    wp_enqueue_script('tilt', 'https://cdnjs.cloudflare.com/ajax/libs/tilt.js/1.2.1/tilt.jquery.min.js', array('jquery'));
    wp_enqueue_script('slick', 'https://cdn.jsdelivr.net/npm/slick-carousel@1.8.1/slick/slick.min.js', array('jquery'));

    wp_enqueue_script('vendors', get_template_directory_uri() . '/core/js/resource_loader.js', array('jquery','tilt'), '0.1.0', true);
    wp_register_script('script_loader', get_template_directory_uri() . '/core/js/script_loader.js', array('jquery', 'vendors'), '0.1.0', true);
    wp_enqueue_script('script_loader');
 
    wp_enqueue_script('ajax-pagination', get_template_directory_uri() . '/src/js/ajax_query_loader.js', array('jquery'), '0.1.0', true);
    wp_localize_script(
        'ajax-pagination',
        'ajaxpagination',
        array(
            'ajaxurl' => admin_url('admin-ajax.php')
        )
    );
}
/* $to = 'sascha-Darius@kniessner.com';
$subject = 'The subject';
$body = 'The email body content';
$headers = array('Content-Type: text/html; charset=UTF-8');

wp_mail($to, $subject, $body, $headers); */

// add ie conditional html5 shim to header
function add_ie_html5_shim()
{
    echo '<!--[if lt IE 9]>';
    echo '<script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>';
    echo '<![endif]-->';
}
add_action('wp_head', 'add_ie_html5_shim');

function sonderzeichen($string){
    $string = strtolower($string);
    $search = array("Ü","ä", "ö", "ü", "ä", "ö", "ü", "ß", "´"," ");
    $replace = array("ue","ae", "oe", "ue", "ae", "oe", "ue", "ss","-","-");
    return str_replace($search, $replace, $string);
}


function _s_add_block_categories( $categories, $post ) {

	return array_merge(
		$categories,
		array(
			array(
				'slug'  => 'morphium-blocks',
				'title' => esc_html__( 'Morphium Blocks', '_s' ),
			),
		)
	);
}
add_filter( 'block_categories', '_s_add_block_categories', 10, 2 );




add_action('acf/init', 'my_acf_blocks_init');
function my_acf_blocks_init() {

    // Check function exists.
    if( function_exists('acf_register_block_type') ) {

        // Register a testimonial block.
        acf_register_block_type(array(
            'name'              => 'testimonial',
            'title'             => __('Testimonial'),
            'description'       => __('A custom testimonial block.'),
            'render_template'   => 'wp_setup/blocks/testimonial.php',
            'category'          => 'morphium-blocks',
            'mode'              => 'preview',
            'supports'          => array(
                'align' => true,
                'mode' => false,
                'jsx' => true,
                'testimonial' => true,
                'anchor'=> true,
                    // Remove the support for wide alignment.
                'alignWide'=> true,
                
            ),
            'post_types' => array('post', 'page'),
            'keywords' => array('quote', 'mention', 'cite'),
          //  'render_callback'   => 'my_acf_block_render_callback',
            'example'           => array(
                'attributes' => array(
                    'mode' => 'preview',
                    'data' => array(
                        'title'   => "Title are...",
                        'category'        => "animation",
                        'background_color'          => "none",
                        'text_color'    => "#eee"
                    )
                )
            )
            
        ));
    }
}


function my_acf_block_render_callback( $block, $content = '', $is_preview = false, $post_id = 0 ) {

    // Create id attribute allowing for custom "anchor" value.
    $id = 'testimonial-' . $block['id'];
    if( !empty($block['anchor']) ) {
        $id = $block['anchor'];
    }

    // Create class attribute allowing for custom "className" and "align" values.
    $className = 'testimonial';
    if( !empty($block['className']) ) {
        $className .= ' ' . $block['className'];
    }
    if( !empty($block['align']) ) {
        $className .= ' align' . $block['align'];
    }

    // Load values and assing defaults.
    $text = get_field('testimonial') ?: 'Your testimonial here...';
    $author = get_field('author') ?: 'Author name';
    $role = get_field('role') ?: 'Author role';
    $image = get_field('image') ?: 295;
    $background_color = get_field('background_color');
    $text_color = get_field('text_color');

    ?>
    <div id="<?php echo esc_attr($id); ?>" class="<?php echo esc_attr($className); ?>">
        <blockquote class="testimonial-blockquote">
            <span class="testimonial-text"><?php echo $text; ?></span>
            <span class="testimonial-author"><?php echo $author; ?></span>
            <span class="testimonial-role"><?php echo $role; ?></span>
        </blockquote>
        <div class="testimonial-image">
            <?php echo wp_get_attachment_image( $image, 'full' ); ?>
        </div>
        <style type="text/css">
            #<?php echo $id; ?> {
                background: <?php echo $background_color; ?>;
                color: <?php echo $text_color; ?>;
            }
        </style>
    </div>
    <?php
}

add_action('acf/init', 'cmplx_morph_register_blocks');
function cmplx_morph_register_blocks() {

    // check function exists.
    if( function_exists('acf_register_block_type') ) {

        // register a testimonial block.
        acf_register_block_type(array(
            'name'              => 'projects_overview',
            'title'             => __('Projekt Übersicht'),
            'category'          => 'morphium-blocks',
            'description'       => __('A custom project block.'),
            'render_callback'   => 'cmplx_morph_block_render_callback'
        ));
    }
}



/**
 * Testimonial Block Callback Function.
 *
 * @param   array $block The block settings and attributes.
 * @param   string $content The block inner HTML (empty).
 * @param   bool $is_preview True during AJAX preview.
 * @param   (int|string) $post_id The post ID this block is saved to.
 */
function cmplx_morph_block_render_callback( $block, $content = '', $is_preview = false, $post_id = 0 ) {

    // Create id attribute allowing for custom "anchor" value.
    $id = 'projects-' . $block['id'];
    if( !empty($block['anchor']) ) {
        $id = $block['anchor'];
    }
    $is_selected = false;
    // Create class attribute allowing for custom "className" and "align" values.
    $className = 'projects';
    if( !empty($block['className']) ) {
        $className .= ' ' . $block['className'];
    }
    if( !empty($block['align']) ) {
        $className .= ' align' . $block['align'];
    }

    // Load values and assing defaults.
    $text = get_field('title') ?: 'Your Title here...';
    $category_slug = get_field('block_category');
    $cat_slug = $category_slug->slug;
    echo $cat_slug;
    $background_color = get_field('background_color');
    $text_color = get_field('text_color');

    ?>
    <div id="<?php echo esc_attr($id); ?>" class="<?php echo esc_attr($className); ?>">
    
 
<div class="section_content media_content row">
    <div class="grid_container container" id="video_grid" data-current="1" data-total="0">

        <div class="grid_controller" id="video_post_filter">
            <div class="categories p-auto m-auto d-flex align-items-end flex-column">
                <?php $categories = get_categories(array( 'hide_empty' => 0, 'orderby' => 'name', 'order' => 'ASC', 'meta_key' => 'ba_checkbox_field_id' )); ?>
                <?php foreach ($categories as $category): ?>
                <div class="category pt-1 pb-1 <?php  if($cat_slug === $category->slug ) { echo 'selected '; $is_selected = true;} ?>"
                    data-cat="<?php echo $category->slug; ?>">
                    <a href="#">
                        <?php echo $category->name; ?>
                    </a>
                </div>
                <?php endforeach;?>
                <div class="category pt-1 pb-1 mt-auto <?php if($is_selected === false) echo 'selected active'; ?>"
                    data-cat="">
                    <a href="#"> Alle </a>
                </div>
            </div>
        </div>

        <div class="row justify-content-md-center" id="loadvideos"></div>


            <a class="prev previuos_posts" id="prev_projectpage" href="#"><i
                    class="fas fa-caret-square-left disabled"></i></a>
            <a class="next next_posts" id="next_projectpage" href="#"><i class="fas fa-caret-square-right"></i></a>

    

        <nav id="grid_pagination">
            <div class="container">
                <a class="prev previuos_posts" id="prev_projectpage" href="#"><i
                        class="fas fa-caret-square-left disabled"></i></a>

                <div id="dots"></div>
                <a class="next next_posts" id="next_projectpage" href="#"><i class="fas fa-caret-square-right"></i></a>
                <div id="page_counter"></div>
            </div>

        </nav>

        <nav class="arrow_nav nav-slide col-md-6 col-lg-4" id="post_controller"> </nav>
    </div>
</div>
    </div>
    <style type="text/css">
            #<?php echo $id; ?> {
                background: <?php echo $background_color; ?>;
                color: <?php echo $text_color; ?>;
                float: left;
                width: 100%;

                }


                span.page-numbers.current {
                    font-size: 1.2rem;
                    margin: 5px;
                }

                a.page-numbers {
                    font-size: 1.2rem;
                    margin: 0 6px;
                    text-align: center;
                }

                nav.pagination {
                    height: 50px;
                    width: 100%;
                    position: relative;
                    right: 0;
                    left: 0;
                    bottom: 0;
                    display: inline-block;
                    float: left;
                    text-align: center;
                }

                .grid_container a.next {

                    display: block;
                    right: 0px;
                    bottom: 15px;
                    width: 50px;
                    position: absolute;
                    padding: 0px;
                    z-index: 22222;
                    top: auto;
                    background: none;
                    color: #63a467;

                }

                a.prev.page-numbers {
                    position: absolute;
                    left: 0;
                    bottom: 15px;
                    display: block;
                }
        </style>
    <?php
}


function referenzeShortcode() {
    ob_start();
    global $post;
    $is_selected = false;
    $cat_slug;?>

<?php   if (get_field('side_cat')):
            $cat_slug = get_field('side_cat');
             $cat_slug = $cat_slug->slug;
        else:
            $cat_slug = $post->post_name;
        endif; ?>


<div class="section_content media_content row">
    <div class="grid_container container" id="video_grid" data-current="1" data-total="0">

        <div class="grid_controller" id="video_post_filter">
            <div class="categories p-auto m-auto d-flex align-items-end flex-column">
                <?php $categories = get_categories(array( 'hide_empty' => 0, 'orderby' => 'name', 'order' => 'ASC', 'meta_key' => 'ba_checkbox_field_id' )); ?>
                <?php foreach ($categories as $category): ?>
                <div class="category pt-1 pb-1 <?php  if($cat_slug === $category->slug ) { echo 'selected '; $is_selected = true;} ?>"
                    data-cat="<?php echo $category->slug; ?>">
                    <a href="#">
                        <?php echo $category->name; ?>
                    </a>
                </div>
                <?php endforeach;?>
                <div class="category pt-1 pb-1 mt-auto <?php if($is_selected === false) echo 'selected active'; ?>"
                    data-cat="">
                    <a href="#"> Alle </a>
                </div>
            </div>
        </div>

        <div class="row justify-content-md-center" id="loadvideos"></div>


            <a class="prev previuos_posts" id="prev_projectpage" href="#"><i
                    class="fas fa-caret-square-left disabled"></i></a>
            <a class="next next_posts" id="next_projectpage" href="#"><i class="fas fa-caret-square-right"></i></a>

    

        <nav id="grid_pagination">
            <div class="container">
                <a class="prev previuos_posts" id="prev_projectpage" href="#"><i
                        class="fas fa-caret-square-left disabled"></i></a>

                <div id="dots"></div>
                <a class="next next_posts" id="next_projectpage" href="#"><i class="fas fa-caret-square-right"></i></a>
                <div id="page_counter"></div>
            </div>

        </nav>

        <nav class="arrow_nav nav-slide col-md-6 col-lg-4" id="post_controller"> </nav>
    </div>
</div>

  <?php
  return ob_get_clean();
}
add_shortcode('references', 'referenzeShortcode' );