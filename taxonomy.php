<?php

get_header(); 

$term = get_term_by( 'slug', get_query_var( 'term' ), get_query_var( 'taxonomy' ) );
echo $term ;
$args= array(
    'post_type' => 'Projekte',
    'posts_per_page'=> -1,
    'post_status'    => 'inherit',
    'posts_per_page'=> -1,
    'taxonomy' => get_query_var( 'term' ),
    'tax_query' => array(
      array (
          'taxonomy' => 'category' ,
          'field' => 'slug',
          'terms' => $term,
      )
    ),

);

$the_query = new WP_Query( $args );

// The Loop
if ( $the_query->have_posts() ) {
	while ( $the_query->have_posts() ) {
      $the_query->the_post();
        echo wp_get_attachment_link($post->ID);
?>
<main id="sub_page" class="content_wrap" role="main" data-offset="200" data-spy="scroll" data-target="#ancher_nav">
  <h1 <?php echo hybrid_get_attr('entry-title  container'); ?>>
    <?php echo $post->post_title ?>
   </h1>
    <?php
   // include(locate_template('wp_setup/components/content.php' ) );
    ?>
</main>

<?php } 
} 

// Reset Post Data
wp_reset_postdata();
?>


<?php get_footer();
?>
