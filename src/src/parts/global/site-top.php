<?php
/**
 * @package shr_commerce
 * @since 1.0
 * @version 1.0
 *
 * This shows the very top of the site with logo and navigation.
 * The navigation is using data-moveto to move itself into the left panel when the site hits a max-width of --nav-move, a css variable of 800px by default
 */

?>

	<div class="site-top-container site-top-nav-wrap ">
		<div class="site-navigation primary-nav container horizontal-menu flex">

			<nav id="primary-navigation" class="main-navigation nav navbar" role="navigation" 
					aria-label="<?php esc_attr_e( 'Primary Navigation', 'shr' ); ?>">
				<div class="container">
					
					<div class="navbar-brand">
						
						<?php shore_site_title_or_logo(); ?>						
						
						<div class="navbar-burger" data-target="navbarExampleTransparentExample">
							<span></span>
							<span></span>
							<span></span>
						</div>
					</div>


						<?php
						wp_nav_menu(
							array(
								'theme_location'  => 'primary',
								'container_class' => 'navbar-menu',
								//'items_wrap'     => '<ul class="navbar-start">%3$s</ul>'

							)
						);?>
						<div class="navbar-end">
							<?php shr_header_cart();	//storefront_product_search();?>
						</div>
						
				</div>
			</nav>

		</div>
	</div>
<header id="masthead" class="site-header site-top hidden" role="banner">
</header>

