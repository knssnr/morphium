jQuery(document).ready(function ($) {

  if ($("#video_post_filter .category.selected").length >= 1) {
    send_post_req(1, $("#video_post_filter .category.selected").data("cat"));
   
  }
  

  $("#video_post_filter .category").click(function (event) {
    event.preventDefault();
    $("#video_post_filter .selected").removeClass("selected");
    $(this).addClass("selected");
    var cat = $(this).data("cat");
    $("#video_grid").data("cat");
    send_post_req(1, cat);
  });

  $(".next.next_posts").click(function (event) {
    event.preventDefault();
    var current = $("#video_grid").data("current");
    var next = current + 1;
    var cat = $("#video_post_filter .selected").first().data("cat")
      ? $("#video_post_filter .selected").first().data("cat")
      : "";
    send_post_req(next, cat);
  });

  $(".prev.previuos_posts").click(function (event) {
    event.preventDefault();
    var current = $("#video_grid").data("current");
    var next = current - 1;
    var cat = $("#video_post_filter .selected").first().data("cat")
      ? $("#video_post_filter  .selected").first().data("cat")
      : "";
    send_post_req(next, cat);
  });

  function send_post_req(page, cat) {
    $.ajax({
      url: ajaxpagination.ajaxurl,
      type: "post",
      data: {
        action: "ajax_pagination",
        category_name: cat,
        page: page,
      },
      success: function (data, textStatus, XMLHttpRequest) {
        var data = JSON.parse(data);
        console.log(page, data);
        $( "#dots" ).html('');
        for (i = 1; i <= data.num_pages; i++) {
          i === page ? $( "#dots" ).append("<span class='dot current'></span>") : $( "#dots" ).append("<span class='dot'></span>");
          
        }
        $('#page_counter').html("<span>"+page+" / "+ data.num_pages+"</span>");


        $('#video_grid').addClass('loaded');
        $("#loadvideos").html("");
        $("#video_grid ").attr("data-total", data.num_pages);
        $("#video_grid ").data("current", page);

        if (page < data.num_pages) {
          $(".next").removeClass('disabled');
        } else {
          $(".next").addClass('disabled');
        }

    

        if (page > 1) {
          $(".prev").removeClass('disabled');
        } else {
          $(".prev").addClass('disabled');
        }
        // console.log("ajax req received: ", data);
        $.each(data.result, function (key, value) {
          var html = value["html"];
          $("#loadvideos").delay(500).append(html);
          $("#loadvideos .grid-item")
            .hide()
            .last()
            .delay(500)
            .fadeIn(key * 500)
            .addClass("loaded")
            .click(function (event) {});

          $("#loadvideos img[data-src]").each(function () {
            var sourceFile = $(this).data("src");
            $(this).attr("src", sourceFile);
            $(this).ready(function () {
              $(this).removeAttr("data-src");
              var w = window.innerWidth;
            });
          });
        });
      },
      error: function (XMLHttpRequest, textStatus, errorThrown) {
        alert(errorThrown);
      },
    });
  }
});
