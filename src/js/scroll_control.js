jQuery(document).ready(function ($) {
  var videos = $("#video_grid");

  $(window).scroll(function () {
    var top_of_element = $(videos).offset().top;
    var half_of_element = $(videos).offset().top + $(videos).outerHeight() / 2;

    var bottom_of_element = $(videos).offset().top + $(videos).outerHeight();

    var bottom_of_screen = $(window).scrollTop() + window.innerHeight;
    var top_of_screen = $(window).scrollTop();

    if (
      top_of_element <= bottom_of_screen &&
      half_of_element >= top_of_screen
    ) {
      $(videos).addClass("visible");
    } else {
      $(videos).removeClass("visible");
    }
  });
});
