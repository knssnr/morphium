<?php 
/* 
* Template Name: Referenzen Display
*/ 
get_header(); ?>
        <?php if( have_rows('sections') ) : ?>
            <main id="sub_page" class="content_wrap" role="main" data-offset="200" data-spy="scroll" data-target="#ancher_nav">
                <h1 <?php echo hybrid_get_attr('entry-title  container'); ?>>
                    <?php the_title(); ?>
                    
                </h1>
                <?php include(locate_template('wp_setup/components/content.php')); ?>
                <?php $r = 0;
                         while ( have_rows('sections') ) : the_row();
                         $r++;
                         include(locate_template('wp_setup/components/sections.php')); ?>
                  <?php endwhile; ?>
            </main>
        <?php endif; ?>

       


<?php get_footer(); ?>
